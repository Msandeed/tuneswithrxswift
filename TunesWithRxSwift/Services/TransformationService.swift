//
//  TransformationService.swift
//  TunesWithRxSwift
//
//  Created by Mostafa Sandeed on 4/8/20.
//  Copyright © 2020 MostafaSandeed. All rights reserved.
//

import Foundation

protocol TransformationServiceProtocol {
    func convert(topAlbumsResponse: TopAlbumsResponse) -> [Album]
}

class TransformationService: TransformationServiceProtocol {
    
    func convert(topAlbumsResponse: TopAlbumsResponse) -> [Album] {
        
        let albums = topAlbumsResponse.topalbums.album
        
        return albums
    }
}

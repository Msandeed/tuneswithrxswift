//
//  ApiService.swift
//  TunesWithRxSwift
//
//  Created by Mostafa Sandeed on 4/8/20.
//  Copyright © 2020 MostafaSandeed. All rights reserved.
//

import Foundation
import RxSwift
import Alamofire
import RxAlamofire

protocol ApiServiceProtocol {
    func getTopAlbums(for artist: String) -> Observable<([Album], NetworkingError?)>
}

class ApiService: ApiServiceProtocol {

    private var transformationService: TransformationServiceProtocol
    
    public init(transformationService: TransformationServiceProtocol = TransformationService()) {
        self.transformationService = transformationService
    }
    
// MARK: - Operations
    func getTopAlbums(for artist: String) -> Observable<([Album], NetworkingError?)> {
        
        guard let url = makeTopAlbumsComponents(withArtist: artist).url else {
            fatalError("ApiService : Couldn't create URL")
        }
        
        return RxAlamofire.requestData(.get, url)
                            .catchError { _ in .empty() }
                            .map { [unowned self] response, data in
                                if response.statusCode == 200 {
                                    do {
                                        let topAlbumsResponse = try JSONDecoder().decode(TopAlbumsResponse.self, from: data)
                                        return (self.parse(topAlbumsResponse: topAlbumsResponse), nil)
                                    } catch {
                                        print("APIService : Could not decode received Data")
                                        return([], .parsing(description: "Could not decode received Data"))
                                    }
                                } else {
                                    do {
                                        let erroneousResponse = try JSONDecoder().decode(ErroneousResponse.self, from: data)
                                        let errorMessage = self.translateError(err: erroneousResponse.error)
                                        print("APIService : \(errorMessage)")
                                        return([], .network(description: errorMessage))
                                    } catch {
                                        let errorMessage = self.translateError(err: 0)
                                        print("APIService : \(errorMessage)")
                                        return([], .network(description: errorMessage))
                                   }
                                    
                                }
                            }
    }
}

// MARK: - URL Factory
extension ApiService {

    func makeTopAlbumsComponents(withArtist artist: String) -> URLComponents {
      var components = URLComponents()
      components.scheme = API.scheme
      components.host = API.host
      components.path = API.path
      
      components.queryItems = [
        URLQueryItem(name: "method", value: "artist.gettopalbums"),
        URLQueryItem(name: "format", value: "json"),
        URLQueryItem(name: "artist", value: artist),
        URLQueryItem(name: "api_key", value: API.key)
      ]
      
      return components
    }
}

// MARK: - Helper Methods
extension ApiService {
    
    private func parse(topAlbumsResponse: TopAlbumsResponse) -> [Album] {
        transformationService.convert(topAlbumsResponse: topAlbumsResponse)
    }

    /* This method translates error codes to explicit messages. Most of those messages should not be shown to the user and should be handled according to the business requirements. However, for the purpose of this demonstration app, they will be shown as alerts in the view. */
    private func translateError(err: Int) -> String {
     
        var errorMessage = ""
        
        switch err {
        case 2:
            errorMessage = "This service does not exist"
        case 3:
            errorMessage = "No method with that name in this package"
        case 4:
            errorMessage = "You do not have permissions to access the service"
        case 5:
            errorMessage = "This service doesn't exist in that format"
        case 6:
            errorMessage = "Your request is missing a required parameter"
        case 7:
            errorMessage = "Invalid resource specified"
        case 8:
            errorMessage = "Something went wrong"
        case 9:
            errorMessage = "Please re-authenticate"
        case 10:
            errorMessage = "You must be granted a valid key by last.fm"
        case 11:
            errorMessage = "This service is temporarily offline. Try again later."
        case 13:
            errorMessage = "Invalid method signature supplied"
        case 16:
            errorMessage = "There was a temporary error processing your request. Please try again"
        case 26:
            errorMessage = "Access for your account has been suspended, please contact Last.fm"
        case 29:
            errorMessage = "Your IP has made too many requests in a short period"
        default:
            errorMessage = "Unknown error"
        }
        
        return errorMessage
    }
}


//
//  NavigationService.swift
//  PicnicNext
//
//  Created by Mostafa Sandeed on 4/13/20.
//  Copyright © 2020 MostafaSandeed. All rights reserved.
//

import UIKit

protocol Coordinator {
    
    var navigationController: UINavigationController { get set }

    func start()
}


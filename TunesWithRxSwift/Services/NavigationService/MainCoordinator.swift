//
//  MainCoordinator.swift
//  PicnicNext
//
//  Created by Mostafa Sandeed on 4/13/20.
//  Copyright © 2020 MostafaSandeed. All rights reserved.
//

import UIKit

protocol MainCoordinatorProtocol {
    func toDetailsView(album: AlbumsListItem)
}

class MainCoordinator: Coordinator, MainCoordinatorProtocol {
    var navigationController: UINavigationController

    init(navigationController: UINavigationController) {
        self.navigationController = navigationController
    }

    func start() {
        let vc = TopAlbumsViewControllerFactory.create(with: self)
        navigationController.pushViewController(vc, animated: false)
    }
    
    func toDetailsView(album: AlbumsListItem) {
//        let vc = DetailsViewControllerFactory.create(with: self, gifId: id)
//        navigationController.pushViewController(vc, animated: true)
    }
}

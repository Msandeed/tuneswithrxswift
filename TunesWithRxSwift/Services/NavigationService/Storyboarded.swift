//
//  Storyboarded.swift
//  PicnicNext
//
//  Created by Mostafa Sandeed on 4/13/20.
//  Copyright © 2020 MostafaSandeed. All rights reserved.
//

import UIKit

protocol Storyboarded {
    static func instantiate(creator: ((NSCoder) -> UIViewController?)?) -> Self
}

extension Storyboarded where Self: UIViewController {
    static func instantiate(creator: ((NSCoder) -> UIViewController?)? = nil) -> Self {
        
        // this pulls out "MyApp.MyViewController"
        let fullName = NSStringFromClass(self)

        // this splits by the dot and uses everything after, giving "MyViewController"
        guard let className = fullName.components(separatedBy: ".").last else {
            fatalError("Cannot find className for \(fullName)")
        }

        // load our storyboard
        let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)

        // instantiate a view controller with that identifier, and force cast as the type that was requested
        guard let result = storyboard.instantiateViewController(identifier: className, creator: creator) as? Self else {
            fatalError("Cannot instantiate \(className)")
        }
        return result
    }
}

//
//  AlbumsRepository.swift
//  TunesWithRxSwift
//
//  Created by Mostafa Sandeed on 4/26/20.
//  Copyright © 2020 MostafaSandeed. All rights reserved.
//

import Foundation
import RxSwift

protocol AlbumsRepository {
    func getTopAlbums(for artist: String) -> Observable<([Album], NetworkingError?)>
}

class RemoteAlbumsRepository: AlbumsRepository {
    
    let apiService: ApiServiceProtocol
    
    public init(apiService: ApiServiceProtocol = ApiService()) {
        self.apiService = apiService
    }
    
    func getTopAlbums(for artist: String) -> Observable<([Album], NetworkingError?)> {
        apiService.getTopAlbums(for: artist)
    }
}

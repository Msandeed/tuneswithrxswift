//
//  ErroneousResponse.swift
//  TunesWithRxSwift
//
//  Created by Mostafa Sandeed on 4/26/20.
//  Copyright © 2020 MostafaSandeed. All rights reserved.
//

import Foundation

// MARK: - ErroneousResponse
class ErroneousResponse: Codable {
    let error: Int
    let message: String

    init(error: Int, message: String) {
        self.error = error
        self.message = message
    }
}

//
//  ApiError.swift
//  TunesWithRxSwift
//
//  Created by Mostafa Sandeed on 4/8/20.
//  Copyright © 2020 MostafaSandeed. All rights reserved.
//

import Foundation

enum NetworkingError: Error {
    case network(description: String)
    case parsing(description: String)
    
    func get() -> String {
        switch self {
        case .network(let description), .parsing(let description):
            return description
        }
    }
}

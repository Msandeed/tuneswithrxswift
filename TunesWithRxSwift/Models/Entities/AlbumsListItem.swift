//
//  AlbumsListItem.swift
//  TunesWithRxSwift
//
//  Created by Mostafa Sandeed on 4/8/20.
//  Copyright © 2020 MostafaSandeed. All rights reserved.
//

import Foundation

struct AlbumsListItem {
 
    let artist: String?
    let mbid: String?
    let name: String?
    let playcount: Int
    let image: [Image]
    
    init(album: Album) {
        artist = album.artist.name
        mbid = album.mbid
        name = album.name
        playcount = album.playcount
        image = album.image
    }

}

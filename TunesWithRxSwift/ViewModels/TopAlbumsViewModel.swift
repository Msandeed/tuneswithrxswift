//
//  TopAlbumsViewModel.swift
//  TunesWithRxSwift
//
//  Created by Mostafa Sandeed on 4/26/20.
//  Copyright © 2020 MostafaSandeed. All rights reserved.
//

import Foundation

import Foundation
import RxSwift
import RxCocoa

class TopAlbumsViewModel {
    
    private let albumsRepository: AlbumsRepository
    
    private var bag = DisposeBag()
    let state: BehaviorRelay<State> = BehaviorRelay(value: .idle)
    var topAlbums: PublishRelay<([AlbumsListItem], NetworkingError?)> = PublishRelay()
    
    init(albumsRepository: AlbumsRepository = RemoteAlbumsRepository()) {
        self.albumsRepository = albumsRepository
    }
    
// MARK: - Event Handling
    func receive(event: Event) {
        switch event {
        case .onAppear:
            _ = albumsRepository.getTopAlbums(for: "Abba")
                .map { albums, error -> ([AlbumsListItem], NetworkingError?) in
                    let albumItems = albums.map { AlbumsListItem(album: $0) }
                    return (albumItems,error)
                }
            .bind(to: topAlbums)
            .disposed(by: bag)
            
            state.accept(.loading)
        case .onAlbumsLoaded:
            state.accept(.loaded)
        case .onItemSelected(let album):
            print("SEARCHVIEWMODEL : Item with name: \(album.name ?? "") selected")
        }
    }
    
}
    
// MARK: - Inner Types
extension TopAlbumsViewModel {
    enum State {
        case idle
        case loading
        case loaded
    }
    
    enum Event {
        case onAppear
        case onAlbumsLoaded
        case onItemSelected(AlbumsListItem)
    }
}


//
//  GIFCollectionViewCell.swift
//  PicnicNext
//
//  Created by Mostafa Sandeed on 4/11/20.
//  Copyright © 2020 MostafaSandeed. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireImage

class AlbumCollectionViewCell: UICollectionViewCell {
    
    static let reuseIdentifer = Identifiers.albumCollectionViewCell

    @IBOutlet weak var albumImageView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    func populate(with album: AlbumsListItem) {
        
        let imageUrl = album.image[3].text
        Alamofire.request(imageUrl).responseImage { response in
            if let image = response.result.value {
                DispatchQueue.main.async {
                    self.albumImageView.image = image
                }
            }
        }
    }
}

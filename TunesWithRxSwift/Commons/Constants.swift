//
//  Constants.swift
//  TunesWithRxSwift
//
//  Created by Mostafa Sandeed on 4/26/20.
//  Copyright © 2020 MostafaSandeed. All rights reserved.
//

import Foundation

struct API {
  static let scheme = "http"
  static let host = "ws.audioscrobbler.com"
  static let path = "/2.0/"
  static let key = "1b55539b76641b65dd15dc411858f4ff"
}

struct Identifiers {
    static let albumCollectionViewCell = "AlbumCollectionViewCell"
}

//
//  ViewController.swift
//  TunesWithRxSwift
//
//  Created by Mostafa Sandeed on 4/8/20.
//  Copyright © 2020 MostafaSandeed. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class TopAlbumsViewControllerFactory {
    class func create(with coordinator: MainCoordinatorProtocol) -> TopAlbumsViewController {
        TopAlbumsViewController.instantiate { coder in
            return TopAlbumsViewController(coder: coder, coordinator: coordinator)
        }
    }
}

class TopAlbumsViewController: BaseViewController, Storyboarded {

    @IBOutlet weak var topAlbumsCollectionView: UICollectionView!
    
    var coordinator: MainCoordinatorProtocol
       
    var bag = DisposeBag()
    var viewModel = TopAlbumsViewModel()
   
    init?(coder: NSCoder, coordinator: MainCoordinatorProtocol) {
       self.coordinator = coordinator
       super.init(coder: coder)
    }

    required init?(coder: NSCoder) {
       fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // UI
        setupTopAlbumsCollectionView()
        
        // Rx
        setupBinding()
        viewModel.receive(event: .onAppear)
    }
    
//MARK: - Rx Setup
    private func setupBinding() {
        stateBinding()
        dataBinding()
    }
    
    private func stateBinding() {
        viewModel.state
            .asObservable()
            .observeOn(MainScheduler.instance)
            .subscribe(onNext: { state in
            switch state {
            case .idle:
                // This is the initial state
                print("TopAlbumsViewController : state : IDLE")
            case .loading:
                print("TopAlbumsViewController : state : LOADING")
                self.showActivityIndicator()
            case .loaded:
                print("TopAlbumsViewController : state : LOADED")
                self.hideActivityIndicator()
            }
        }).disposed(by: bag)
    }
    
    private func dataBinding() {
        
        // To display error in Alert View and fire .onAlbumsLoaded event in VM
        viewModel.topAlbums
                 .asObservable()
                 .observeOn(MainScheduler.instance)
                 .subscribe(onNext: { [unowned self] _,error in
                    self.viewModel.receive(event: .onAlbumsLoaded)
                    if let error = error {
                        self.showNotificationAlert(title: "Error!", message: error.get())
                    }
                }).disposed(by: bag)
        
        // Bind to CollectionView
        viewModel.topAlbums
                 .asObservable()
                 .observeOn(MainScheduler.instance)
                 .map { albums, _ in
                    return albums
                 }
                 .bind(to: topAlbumsCollectionView.rx.items(cellIdentifier: AlbumCollectionViewCell.reuseIdentifer)) { (index, album, cell: AlbumCollectionViewCell) in
                    cell.populate(with: album)
                 }.disposed(by: bag)
            
    }
}


//
//  TopAlbumsVC + CollectionView.swift
//  TunesWithRxSwift
//
//  Created by Mostafa Sandeed on 4/26/20.
//  Copyright © 2020 MostafaSandeed. All rights reserved.
//

import UIKit

extension TopAlbumsViewController {

//MARK: - CollectionView basic configuration
    func setupTopAlbumsCollectionView() {
        
        topAlbumsCollectionView.register(UINib(nibName: AlbumCollectionViewCell.reuseIdentifer, bundle: nil), forCellWithReuseIdentifier: AlbumCollectionViewCell.reuseIdentifer)

        topAlbumsCollectionView.collectionViewLayout = createLayout()
        handleTaps()
    }
    
//MARK: - CollectionView Layout
    private func createLayout() -> UICollectionViewLayout {
   
        let itemSize = NSCollectionLayoutSize(widthDimension: .fractionalWidth(1/3),
                                             heightDimension: .fractionalHeight(1.0))
        let item = NSCollectionLayoutItem(layoutSize: itemSize)
        item.contentInsets = NSDirectionalEdgeInsets(top: 5, leading: 5, bottom: 5, trailing: 5)

        let groupSize = NSCollectionLayoutSize(widthDimension: .fractionalWidth(1.0),
                                              heightDimension: .fractionalWidth(1/3))
        let group = NSCollectionLayoutGroup.horizontal(layoutSize: groupSize,
                                                         subitems: [item])

        let section = NSCollectionLayoutSection(group: group)

        let layout = UICollectionViewCompositionalLayout(section: section)
        return layout
    }
}

//MARK: - CollectionView Interaction
extension TopAlbumsViewController {
    func handleTaps() {
        topAlbumsCollectionView
        .rx
        .modelSelected(AlbumsListItem.self)
        .subscribe(onNext: { [unowned self] albumItem in

            if let selectedItemIndexPath = self.topAlbumsCollectionView.indexPathsForSelectedItems?.first {
                self.topAlbumsCollectionView.deselectItem(at: selectedItemIndexPath, animated: true)
            }
            
            self.viewModel.receive(event: .onItemSelected(albumItem))
            self.coordinator.toDetailsView(album: albumItem)
        })
        .disposed(by: bag)
    }
}

//
//  BaseViewController.swift
//  PicnicNext
//
//  Created by Mostafa Sandeed on 4/10/20.
//  Copyright © 2020 MostafaSandeed. All rights reserved.
//

import UIKit

class BaseViewController: UIViewController {

    var activityIndicator = UIActivityIndicatorView()

    override func viewDidLoad() {
        super.viewDidLoad()

        setupAppTheme()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }

// MARK: - Navigation bar styling
    private func setupAppTheme() {
        
        self.navigationController?.navigationBar.barTintColor = .systemIndigo
        self.navigationController?.navigationBar.tintColor = UIColor.label
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
    }

// MARK: - Activity indicator control
    func showActivityIndicator() {
        self.activityIndicator.center = self.view.center
        self.activityIndicator.layer.bounds.size = CGSize(width: 50, height: 50)
        self.activityIndicator.backgroundColor = UIColor.clear
        self.activityIndicator.startAnimating()
        self.view.addSubview(activityIndicator)
    }

    func hideActivityIndicator() {
        activityIndicator.stopAnimating()
        activityIndicator.removeFromSuperview()
    }

// MARK: - Generic notification alert
    func showNotificationAlert (title: String, message: String?) {

        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)

        let dismiss = UIAlertAction(title: "OK", style: .default, handler: { _ in
            alert.dismiss(animated: true, completion: nil)
        })

        alert.addAction(dismiss)
        present(alert, animated: true, completion: nil)

    }

}


//
//  AppDelegate.swift
//  TunesWithRxSwift
//
//  Created by Mostafa Sandeed on 4/8/20.
//  Copyright © 2020 MostafaSandeed. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    var window: UIWindow?
    var coordinator: MainCoordinator?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        
        let navController = UINavigationController()
       coordinator = MainCoordinator(navigationController: navController)
       coordinator?.start()
         
       window = UIWindow(frame: UIScreen.main.bounds)
       window?.rootViewController = navController
       window?.makeKeyAndVisible()
        
        return true
    }
}

